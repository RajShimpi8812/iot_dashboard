import React, { useState, useEffect } from 'react';

const MetabaseComponent = () => {
  const [iframeUrl, setIframeUrl] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchIframeUrl = async () => {
      try {
        const response = await fetch('http://35.244.32.209:3000/client-dashboard/data/71');
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json(); // Adjust according to your actual data structure
        setIframeUrl(data.iframeUrl); // Assuming the URL you need is in data.url
      } catch (error) {
        setError(error.message);
        console.error('Error fetching data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchIframeUrl();
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <div>
      {iframeUrl ? (
        <iframe
          src={iframeUrl}
          width="100%"
          height="800px"
          frameBorder="0"
          allowTransparency="true"
          allowFullScreen
        ></iframe>
      ) : (
        <p>Loading...</p> // This line may be redundant now since we handle loading state separately
      )}
    </div>
  );
};

export default MetabaseComponent;
